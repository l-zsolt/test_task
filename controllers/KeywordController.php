<?php

namespace app\controllers;

use Yii;
use yii\base\Exception;

class KeywordController extends \yii\web\Controller
{
    public static $keywordArr = [];

    public function actionIndex()
    {
        self::readJsonKeywordParam();

        return $this->render('index', [
            'preDefTags' => self::$keywordArr
        ]);
    }

    public function create()
    {
        die('halio');
    }

    public function actionCreate()
    {
        self::readJsonKeywordParam();

        if (Yii::$app->request->post('parentName')) {
            if (!preg_match('/^[a-zA-Z0-9]*$/', Yii::$app->request->post('parentName'))) {
                throw new Exception('Invalid tag character.');
            } elseif (isset(self::$keywordArr[Yii::$app->request->post('parentName')])) {
                throw new Exception('Parent tag is exists.');
            }

            self::$keywordArr[Yii::$app->request->post('parentName')] = [];
        } elseif (Yii::$app->request->post('childName')) {
            $parentArr = array_keys(Yii::$app->request->post('childName'));
            $parentName = end($parentArr);

            if (!preg_match('/^[a-zA-Z0-9]*$/', Yii::$app->request->post('childName')[$parentName])) {
                throw new Exception('Invalid tag character.');
            }

            self::$keywordArr[$parentName][Yii::$app->request->post('childName')[$parentName]] = null;
        }

        self::saveJsonKeywordParam();

        return $this->redirect(['keyword/index']);
    }

    public function actionUpdate()
    {
        self::readJsonKeywordParam();

        if (Yii::$app->getRequest()->getQueryParam('child')) {
            if (!preg_match('/^[a-zA-Z0-9]*$/', Yii::$app->getRequest()->getQueryParam('child'))) {
                throw new Exception('Invalid tag character.');
            }

            unset(self::$keywordArr[Yii::$app->getRequest()->getQueryParam('parent')][Yii::$app->getRequest()->getQueryParam('child')]);
            self::$keywordArr[Yii::$app->getRequest()->getQueryParam('parent')][Yii::$app->getRequest()->post('newName')] = null;
        } elseif (Yii::$app->getRequest()->getQueryParam('parent')) {
            if (!preg_match('/^[a-zA-Z0-9]*$/', Yii::$app->getRequest()->getQueryParam('parent'))) {
                throw new Exception('Invalid tag character.');
            }

            self::$keywordArr[Yii::$app->getRequest()->post('newName')] = self::$keywordArr[Yii::$app->getRequest()->getQueryParam('parent')];
            unset(self::$keywordArr[Yii::$app->getRequest()->getQueryParam('parent')]);
        }

        self::saveJsonKeywordParam();

        return $this->redirect(['keyword/index']);
    }

    public function actionDelete()
    {
        self::readJsonKeywordParam();

        if (Yii::$app->getRequest()->getQueryParam('child')) {
            unset(self::$keywordArr[Yii::$app->getRequest()->getQueryParam('parent')][Yii::$app->getRequest()->getQueryParam('child')]);
        } elseif (Yii::$app->getRequest()->getQueryParam('parent')) {
            unset(self::$keywordArr[Yii::$app->getRequest()->getQueryParam('parent')]);
        }

        self::saveJsonKeywordParam();

        return $this->redirect(['keyword/index']);
    }

    public static function readJsonKeywordParam()
    {
        $content = file_get_contents(__DIR__ . '/../config/keywords.txt');
        self::$keywordArr = json_decode($content, true);
    }

    private static function saveJsonKeywordParam()
    {
        file_put_contents(__DIR__ . '/../config/keywords.txt', json_encode(self::$keywordArr));
    }
}
