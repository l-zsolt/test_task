<?php

namespace app\controllers;

use Yii;
use yii\base\Exception;
use yii\data\Pagination;

class FlickrController extends \yii\web\Controller
{
    private $apiKey = '57f694132e4714c29a64c9af890b124e';
    private $apiUrl;
    private $apiResponse;
    private $searchResult;

    private $pageSize = 9;
    private $photoSize = ['q', 'b'];
    private $defaultKeyword = 'volvo';

    /**
     * @return string
     * @throws Exception
     */
    public function actionIndex()
    {
        KeywordController::readJsonKeywordParam();

        $pageNumer = Yii::$app->request->get('page');

        if (!empty(Yii::$app->request->post('searchName'))) {
            Yii::$app->session->set('searchKeyword', Yii::$app->request->post('searchName'));
        }

        if (
            Yii::$app->session->has('searchKeyword') &&
            preg_match('/^[a-zA-Z0-9,]*$/', Yii::$app->session->get('searchKeyword'))
        ) {
            $keyword = Yii::$app->session->get('searchKeyword');
        } else {
            Yii::$app->session->remove('searchKeyword');
            $keyword = $this->defaultKeyword;
        }

        $keyword = str_replace(',', '%2C', $keyword);

        $this->flilckrInit($keyword, $pageNumer, $this->pageSize);
        $this->flickrRequestSend();

        $this->getPhotoDatas(['PhotoUrl', 'Title', 'Description', 'Tags']);

        $pagination = new Pagination(['totalCount' => $this->apiResponse->photos->total, 'pageSize' => $this->pageSize]);

        return $this->render('index', [
            'searchResult' => (array) $this->searchResult,
            'thumbnailSizeId' => $this->photoSize[0],
            'largeSizeId' => end($this->photoSize),
            'pagination' => $pagination,
            'preDefTags' => KeywordController::$keywordArr
        ]);
    }

    public function actionDefault()
    {
        Yii::$app->session->remove('searchKeyword');
        return $this->redirect(['flickr/index']);
    }

    /**
     * @param $keyword
     * @param $page
     * @param $perPage
     */
    private function flilckrInit($keyword, $page, $perPage)
    {
        $this->apiUrl = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key={$this->apiKey}&tags={$keyword}&extras=tags%2Cdescription&per_page={$perPage}&page={$page}&format=json&nojsoncallback=1";
    }

    /**
     * 
     */
    private function flickrRequestSend()
    {
        if (!empty($this->apiUrl)) {
            $this->apiResponse = json_decode(file_get_contents($this->apiUrl));
        }
    }

    /**
     * @param $callFuncList
     * @throws Exception
     */
    private function getPhotoDatas($callFuncList)
    {
        // Check exists parameter function
        foreach ($callFuncList as $funcName) {
            $callFuncName = 'get' . $funcName;

            if (!method_exists($this, $callFuncName)) {
                throw new Exception('Undefined photo parameter function.');
            }
        }

        foreach($this->apiResponse->photos->photo as $photoItem){
            $this->createResultElement($photoItem->id);

            foreach ($callFuncList as $funcName) {
                $callFuncName = 'get' . $funcName;
                $this->$callFuncName($photoItem);
            }
        }
    }

    /**
     * @param $id
     */
    private function createResultElement($id)
    {
        if (!isset($this->searchResult[$id])) {
            $this->searchResult[$id] = [];
        }
    }

    /**
     * @param $imgObj
     */
    private function getPhotoUrl($imgObj)
    {
        foreach ($this->photoSize as $sizeLetter) {
            $this->searchResult[$imgObj->id]['urls'][$sizeLetter] = "https://farm{$imgObj->farm}.staticflickr.com/{$imgObj->server}/{$imgObj->id}_{$imgObj->secret}_{$sizeLetter}.jpg";
        }
    }

    /**
     * @param $imgObj
     */
    private function getFileSize($imgObj)
    {
        // Get largest photo type size
        $headerData = get_headers($this->searchResult[$imgObj->id]['urls'][end($this->photoSize)], 1);

        if (is_numeric($headerData['Content-Length'])) {
            $this->searchResult[$imgObj->id]['fileSize'] = round(($headerData['Content-Length'] / 1024), 2); // change to kilobyte
        } else {
            $this->searchResult[$imgObj->id]['fileSize'] = null;
        }
    }

    /**
     * @param $imgObj
     */
    private function getTags($imgObj)
    {
        if (!empty($imgObj->tags)) {
            $this->searchResult[$imgObj->id]['tags'] = explode(' ', $imgObj->tags);
        } else {
            $this->searchResult[$imgObj->id]['tags'] = [];
        }
    }

    /**
     * @param $imgObj
     */
    private function getTitle($imgObj)
    {
        $this->searchResult[$imgObj->id]['title'] = $imgObj->title;
    }

    /**
     * @param $imgObj
     */
    private function getDescription($imgObj)
    {
        if (!empty($imgObj->description->_content)) {
            $this->searchResult[$imgObj->id]['description'] = $imgObj->description->_content;
        } else {
            $this->searchResult[$imgObj->id]['description'] = null;
        }
    }
}
