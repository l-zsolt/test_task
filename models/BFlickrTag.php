<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "b_flickr_tag".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent
 */
class BFlickrTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'b_flickr_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent'], 'required'],
            [['parent'], 'integer'],
            [['name'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'parent' => 'Parent',
        ];
    }
}
