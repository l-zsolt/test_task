<?php

use yii\db\Migration;

class m171016_174931_load_test_data_b_flickr_tag extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->insert('b_flickr_tag', array(
            'id' => 1,
            'name' => 'Car',
            'parent' => 0
        ));

        $this->insert('b_flickr_tag', array(
            'id' => 2,
            'name' => 'Bus',
            'parent' => 0
        ));

        $this->insert('b_flickr_tag', array(
            'id' => 3,
            'name' => 'BMW',
            'parent' => 1
        ));

        $this->insert('b_flickr_tag', array(
            'id' => 4,
            'name' => 'Opel',
            'parent' => 1
        ));

        $this->insert('b_flickr_tag', array(
            'id' => 5,
            'name' => 'Ikarus',
            'parent' => 2
        ));

        $this->insert('b_flickr_tag', array(
            'id' => 6,
            'name' => 'Volvo',
            'parent' => 2
        ));
    }

    public function down()
    {
        echo "m171016_174931_load_test_data_b_flickr_tag cannot be reverted.\n";

        return false;
    }

}
