<?php

use yii\db\Schema;
use yii\db\Migration;

class m171016_173733_b_flickr_tag extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('b_flickr_tag', array(
            'id' => 'pk',
            'name' => 'VARCHAR(128)',
            'parent' => 'INT NOT NULL'
        ));
    }

    public function down()
    {
        $this->dropTable('b_flickr_tag');
    }

}
