<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->registerJsFile(
    '@web/js/site.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

?>
<div class="flickr-index">

    <div class="row search-field">

        <?= Html::beginForm(['/flickr/index'], 'POST'); ?>

        <div class="col-lg-6">
            <div class="input-group">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Choose tag <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <?php

                        foreach ($preDefTags as $parentTagName => $childTag) {
                            ?><li class="pre-tag-bg"><a class="pre-keyword" data-keyword="<?= $parentTagName ?>" href="#"><?= $parentTagName ?></a></li><?php

                            foreach (array_keys($childTag) as $childTagName) {
                                ?><li><a class="pre-keyword" data-keyword="<?= $childTagName ?>" href="#"><?= $childTagName ?></a></li><?php
                            }
                        }

                        ?>
                    </ul>
                </div>
                <input name="searchName" type="text" class="form-control search-tag" placeholder="Search by tag..." value="<?= Yii::$app->session->get('searchKeyword'); ?>">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">Search</button>
                    <a href="/flickr/default" class="btn btn-default">Default</a>
                </span>
            </div>
        </div>

        <?= Html::endForm() ?>

    </div>

    <div class="row">
    <?php
    $i = 0;

    foreach ($searchResult as $photoData) {
        $i++;
    ?>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <img class="photo-item" data-original="<?= $photoData['urls'][$largeSizeId] ?>" src="<?= $photoData['urls'][$thumbnailSizeId] ?>" alt="<?= $photoData['title'] ?>">

                <div class="caption">
                    <h3><?= $photoData['title'] ?></h3>
                    <p><?= $photoData['description'] ?></p>
                </div>

                <?php foreach ($photoData['tags'] as $tag) { ?>
                    <span class="label label-default"><?= $tag ?></span>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    </div>

    <div class="clear"></div>

    <div class="text-center">
    <?php

    echo \yii\widgets\LinkPager::widget([
        'pagination' => $pagination,
    ]);

    ?>
    </div>
</div><!-- flickr-index -->