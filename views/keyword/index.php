<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->registerJsFile(
    '@web/js/site.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);

?>
<div class="container">
    <h2>Tag manager</h2>

    <table class="table">
        <thead>
            <tr>
                <th>Parent tag name</th>
                <th>Children tag name</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>

        <?php foreach ($preDefTags as $parentTagName => $childTag) { ?>
            <tr class="active">
                <td>
                    <?= Html::beginForm(['/keyword/' . $parentTagName], 'PUT', ['class' => 'new-name']); ?>
                    <div class="col-lg-5">
                        <div class="input-group">
                            <input type="text" name="newName" class="form-control" value="<?= $parentTagName ?>">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Update</button>
                            </span>
                        </div>
                    </div>
                    <?= Html::endForm() ?>
                </td>
                <td></td>
                <td>
                    <a href="/keyword/<?= $parentTagName ?>" data-method="delete"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                </td>
            </tr>

        <?php foreach (array_keys($childTag) as $childTagName) { ?>
            <tr>
                <td></td>
                <td>
                    <?= Html::beginForm(['/keyword/' . $parentTagName . '/' . $childTagName], 'PUT', ['class' => 'new-name']); ?>
                    <div class="col-lg-5">
                        <div class="input-group">
                            <input type="text" name="newName" class="form-control" value="<?= $childTagName ?>">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Update</button>
                            </span>
                        </div>
                    </div>
                    <?= Html::endForm() ?>
                </td>
                <td>
                    <a href="/keyword/<?= $parentTagName ?>/<?= $childTagName ?>" data-method="delete"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td colspan="3">
                    <?= Html::beginForm(['/keyword'], 'POST'); ?>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">Add new child:</span>
                        <input type="text" name="childName[<?= $parentTagName ?>]" class="form-control" placeholder="New child name">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Save</button>
                        </span>
                    </div>
                    <?= Html::endForm() ?>
                </td>
            </tr>
        <?php } ?>
            <tr class="active">
                <td colspan="3">
                    <?= Html::beginForm(['/keyword'], 'POST'); ?>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">Add new parent:</span>
                        <input type="text" name="parentName" class="form-control" placeholder="New parent name">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Save</button>
                        </span>
                    </div>
                    <?= Html::endForm() ?>
                </td>
            </tr>
        </tbody>
    </table>
</div>